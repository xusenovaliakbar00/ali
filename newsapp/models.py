from django.db import models

# Create your models here.
from django.utils import timezone
from django.shortcuts import reverse

class Category(models.Model):
    title = models.CharField('Category', max_length=255,
    db_index=True)
    slug = models.SlugField('Manzil',unique=True)


    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})


class Post(models.Model):
    title = models.CharField('Sarlavha', max_length=255, db_index=True)
    slug = models.SlugField('Manzil', unique=True)
    description = models.CharField('Qisqacha malumot', max_length=255, db_index=True)
    # full_info = models.TextField("toliq malumot", db_index=True)
    image = models.ImageField('Rasm', db_index=True)
    date = models.DateField('sana', default=timezone.now)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, related_name='categories')
    popular = models.IntegerField('popular', default=1)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Postlar"

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('news_detail_url', kwargs={'slug':self.slug})




class Comment(models.Model):
    news = models.ForeignKey(Post, on_delete=models.CASCADE,null=True,related_name='comments')
    author_name=models.CharField('author_name',max_length=50)
    comment_text=models.TextField('comment',max_length=1000)


    def __str__(self):
        return 'USER:'+self.author_name+'--'+self.comment_text
    

    class Meta:
        verbose_name ='comments'
        verbose_name_plural ='comments'


